org 100h

jmp mulai       

msg:    db      "(1) Tambah",0dh,0ah,"(2) Kali",0dh,0ah,"(3) Kurang",0dh,0ah,"(4) Bagi", 0Dh,0Ah, '$'
msg2:    db      0dh,0ah,"masukkan angka pertama : $"
msg3:    db      0dh,0ah,"masukkan angka kedua : $"
msg4:    db      0dh,0ah,"Error!!!... $" 
msg5:    db      0dh,0ah,"hasil : $" 
msg6:    db      0dh,0ah ,'terima kasih ', 0Dh,0Ah, '$'

mulai:  mov ah,9
        mov dx, offset msg 
        int 21h
        mov ah,0                       
        int 16h  
        cmp al,31h 
        je Penambahan
        cmp al,32h
        je Perkalian
        cmp al,33h
        je Pengurangan
        cmp al,34h
        je Pembagian
        mov ah,09h
        mov dx, offset msg4
        int 21h
        mov ah,0
        int 16h
        jmp mulai
        
Penambahan: mov ah,09h  
            mov dx, offset msg2  
            int 21h
            mov cx,0 
            call MasukNo  
            push dx
            mov ah,9
            mov dx, offset msg3
            int 21h 
            mov cx,0
            call MasukNo
            pop bx
            add dx,bx
            push dx 
            mov ah,9
            mov dx, offset msg5
            int 21h
            mov cx,1000
            pop dx
            call Lihat 
            jmp Keluar 
            
MasukNo:    mov ah,0
            int 16h 
            mov dx,0  
            mov bx,1 
            cmp al,0dh 
            je DariNo 
            sub ax,30h 
            call LihatNo 
            mov ah,0 
            push ax  
            inc cx   
            jmp MasukNo     
   
DariNo:     pop ax  
            push dx      
            mul bx
            pop dx
            add dx,ax
            mov ax,bx       
            mov bx,10
            push dx
            mul bx
            pop dx
            mov bx,ax
            dec cx
            cmp cx,0
            jne DariNo
            ret   
              
Lihat:  mov ax,dx
       mov dx,0
       div cx 
       call LihatNo
       mov bx,dx 
       mov dx,0
       mov ax,cx 
       mov cx,10
       div cx
       mov dx,bx 
       mov cx,ax
       cmp ax,0
       jne Lihat
       ret


LihatNo:    push ax 
           push dx 
           mov dx,ax 
           add dl,30h 
           mov ah,2
           int 21h
           pop dx  
           pop ax
           ret
      
   
Keluar: mov dx,offset msg6
        mov ah, 09h
        int 21h  
        mov ah, 0
        int 16h
        ret
            
                       
Perkalian:  mov ah,09h
            mov dx, offset msg2
            int 21h
            mov cx,0
            call MasukNo
            push dx
            mov ah,9
            mov dx, offset msg3
            int 21h 
            mov cx,0
            call MasukNo
            pop bx
            mov ax,dx
            mul bx 
            mov dx,ax
            push dx 
            mov ah,9
            mov dx, offset msg5
            int 21h
            mov cx,1000
            pop dx
            call Lihat 
            jmp Keluar 


Pengurangan:mov ah,09h
            mov dx, offset msg2
            int 21h
            mov cx,0
            call MasukNo
            push dx
            mov ah,9
            mov dx, offset msg3
            int 21h 
            mov cx,0
            call MasukNo
            pop bx
            sub bx,dx
            mov dx,bx
            push dx 
            mov ah,9
            mov dx, offset msg5
            int 21h
            mov cx,1000
            pop dx
            call Lihat 
            jmp Keluar 
    
            
Pembagian:  mov ah,09h
            mov dx, offset msg2
            int 21h
            mov cx,0
            call MasukNo
            push dx
            mov ah,9
            mov dx, offset msg3
            int 21h 
            mov cx,0
            call MasukNo
            pop bx
            mov ax,bx
            mov cx,dx
            mov dx,0
            mov bx,0
            div cx
            mov bx,dx
            mov dx,ax
            push bx 
            push dx 
            mov ah,9
            mov dx, offset msg5
            int 21h
            mov cx,1000
            pop dx
            call Lihat
            pop bx
            cmp bx,0
            je Keluar 
            jmp Keluar             
